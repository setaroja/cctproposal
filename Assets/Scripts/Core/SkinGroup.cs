﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[RequireComponent(typeof(Entity))]
public class SkinGroup : MonoBehaviour, ISaveLoad
{
	[InlineEditor(InlineEditorModes.LargePreview)]
	[AssetsOnly]
	[LabelText("Shared data")]
	public SkinGroupData skinGroupData;
	
	[SerializeField] private int currentSkinIndex = -1;
		
	private Skin skin = null;
	private bool isModified = false;
	private bool isVisible = false;

	//-------------------------------------------------------------------------------------------------
	private void Start()
	{
		if (Application.isPlaying)
		{
			AddVisibilityCollider();
		}
	}

	//-------------------------------------------------------------------------------------------------
	private void AddVisibilityCollider()
	{
		var coll = gameObject.AddComponent<SphereCollider>();
		coll.radius = 5;
		coll.isTrigger = true;
	}

	//-------------------------------------------------------------------------------------------------
	public void LoadSkin()
	{
		// TODO: check if is the same skin!
		
		if (SkinIsValid(currentSkinIndex) && skinGroupData.prefabSkins[currentSkinIndex])
		{
			UnloadSkin();
			skin = Instantiate(skinGroupData.prefabSkins[currentSkinIndex], transform);
			skin.transform.localPosition = Vector3.zero;
		
			skin.gameObject.SetActive(isVisible);
		}
	}
	
	//-------------------------------------------------------------------------------------------------
	public void UnloadSkin()
	{
		if (skin)
		{
#if UNITY_EDITOR
			if(Application.isPlaying)
				Destroy(skin.gameObject);
			else
				DestroyImmediate(skin.gameObject);
#else			
			Destroy(skin.gameObject);
#endif
			skin = null;
		}
	}

	//-------------------------------------------------------------------------------------------------
	public bool IsSkinLoaded()
	{
		return skin != null ? true : false;
	}
	
	//-------------------------------------------------------------------------------------------------
	public bool SkinIsValid(int skinIndex)
	{
		return skinIndex >= 0 &&
			   skinGroupData && 
		       skinGroupData.prefabSkins != null && 
		       skinIndex < skinGroupData.prefabSkins.Count && 
		       skinGroupData.prefabSkins[skinIndex] != null;
	}

	//-------------------------------------------------------------------------------------------------
	public int GetSkinIndex()
	{
		return currentSkinIndex;
	}
	
	//-------------------------------------------------------------------------------------------------
	public void SetSkinIndex(int skinIndex)
	{
		if (SkinIsValid(skinIndex))
		{
			currentSkinIndex = skinIndex;
			LoadSkin();
			
			if (Application.isPlaying)
			{
				isModified = true;
			}
		}
	}

	//-------------------------------------------------------------------------------------------------
	[ContextMenu ("Random skin")]
	public void SetRandomSkin()
	{
		SetSkinIndex(Random.Range(0, skinGroupData.prefabSkins.Count));
	}

	//-------------------------------------------------------------------------------------------------
	public void SetVisible(bool visible)
	{
		isVisible = visible;
		if (skin)
		{
			skin.gameObject.SetActive(isVisible);
		}
	}

	//-------------------------------------------------------------------------------------------------
	public bool IsVisible()
	{
		return isVisible;
	}

	//-------------------------------------------------------------------------------------------------
	void OnTouch()
	{
		SetRandomSkin();
	}

	//-------------------------------------------------------------------------------------------------
	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("MainCamera"))
		{
			LoadSkin();
			SetVisible(true);
		}
	}
	
	//-------------------------------------------------------------------------------------------------
	private void OnTriggerExit(Collider other)
	{
		if (other.CompareTag("MainCamera"))
		{
			UnloadSkin();
			SetVisible(false);
		}
	}	


	#region ISaveLoad
	
	//-------------------------------------------------------------------------------------------------
	class Data : ComponentData
	{
		public int currentSkin;
	}
	
	//-------------------------------------------------------------------------------------------------
	public ComponentData GetSaveData()
	{
		Data data = new Data { currentSkin = this.currentSkinIndex };

		return data;
	}

	//-------------------------------------------------------------------------------------------------
	public void SetSaveData(ComponentData componentData)
	{
		var data = (Data) componentData;
		SetSkinIndex(data.currentSkin);
		
		isModified = true;
	}

	//-------------------------------------------------------------------------------------------------
	public bool IsModified()
	{
		return isModified;
	}

	#endregion	
}
