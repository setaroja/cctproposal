﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ComponentData
{
    public string name;
}

[System.Serializable]
public class GameObjectData
{
    public string uniqueId;
    public List<ComponentData> components = new List<ComponentData>();
}

[System.Serializable]
public class SceneData
{
    public List<GameObjectData> gameObjects = new List<GameObjectData>();
}
