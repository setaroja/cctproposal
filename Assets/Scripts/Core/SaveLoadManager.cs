﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;

public static class SaveLoadManager
{
    //-------------------------------------------------------------------------------------------------
    public static void Save()
    {
        SceneData sceneData = new SceneData();
        
        var entities = GameObject.FindObjectsOfType<Entity>();
        foreach (var entity in entities)
        {
            bool objectRequiresSave = false;
            GameObjectData gameObjectData = new GameObjectData(){ uniqueId = entity.uniqueId };
            
            var components = entity.GetComponents<ISaveLoad>();
            foreach (var component in components)
            {
                if (component.IsModified())
                {
                    var componentData = component.GetSaveData();
                    if (componentData != null)
                    {
                        componentData.name = component.GetType().ToString();
                        gameObjectData.components.Add(componentData);
                        objectRequiresSave = true;
                    }
                }
            }

            if (objectRequiresSave)
            {
                sceneData.gameObjects.Add(gameObjectData);
            }
        }

        JsonSerializerSettings settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.Auto };
        var json = JsonConvert.SerializeObject(sceneData, Formatting.Indented, settings);
        Debug.Log(json);

        string filePath = Application.persistentDataPath + "/SaveGame.json";
        File.WriteAllText(filePath, json);
    }
    
    //-------------------------------------------------------------------------------------------------
    public static void Load()
    {
        var entitiesMap = GetAllEntitiesInScene();

        string filePath = Application.persistentDataPath + "/SaveGame.json";
        var json = File.ReadAllText(filePath);
        
        JsonSerializerSettings settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.Auto };
        var sceneData = JsonConvert.DeserializeObject<SceneData>(json, settings);

        foreach (var goData in sceneData.gameObjects)
        {
            Entity entity;
            if (entitiesMap.TryGetValue(goData.uniqueId, out entity))
            {
                foreach (var componentData in goData.components)
                {
                    ISaveLoad component = entity.GetComponent(componentData.name) as ISaveLoad;
                    component.SetSaveData(componentData);
                }
            }
            else
            {
                Debug.LogError("Load state: Entity " + goData.uniqueId + " not found. Ignored");
            }
        }
    }

    //-------------------------------------------------------------------------------------------------
    private static Dictionary<string, Entity> GetAllEntitiesInScene()
    {
        Dictionary<string, Entity> entitiesMap = new Dictionary<string, Entity>();
        var entities = UnityEngine.Object.FindObjectsOfType<Entity>();
        foreach (var entity in entities)
        {
            entitiesMap.Add(entity.uniqueId, entity);
        }

        return entitiesMap;
    }
}
