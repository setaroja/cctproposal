﻿using Sirenix.OdinInspector.Editor;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SkinGroup))]
public class SkinGroupEditor : OdinEditor
{
    private SkinGroup skinGroup;

    //-------------------------------------------------------------------------------------------------
    private void OnEnable()
    {
        if (EditorUtils.IsPrefab(target)) return;

        skinGroup = (target as SkinGroup);

        if (!Application.isPlaying &&!skinGroup.IsSkinLoaded())
        {
            skinGroup.LoadSkin();
            skinGroup.SetVisible(true);
        }
    }

    //-------------------------------------------------------------------------------------------------
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        
        if (EditorUtils.IsPrefab(target)) return;

    }
}
