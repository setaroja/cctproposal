﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skin : MonoBehaviour 
{
    void Start()
    {
        transform.localScale = Vector3.zero;
        transform.localPosition = transform.localPosition + new Vector3(0, 5, 0);
    }

    private void Update()
    {
        transform.localScale += new Vector3(3, 3, 3) * Time.deltaTime;
        if (transform.localScale.x > 1)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
        
        transform.localPosition -= new Vector3(0, 15, 0) * Time.deltaTime;
        if (transform.localPosition.y < 0)
        {
            transform.localPosition= Vector3.zero;
        }        
    }
}
