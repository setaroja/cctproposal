﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(menuName="Skin Group Data")]
public class SkinGroupData : ScriptableObject
{
    [AssetsOnly]
    [InlineEditor(InlineEditorModes.LargePreview)]
    public List<Skin> prefabSkins;
    
}
