﻿using System;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public static class EntityManager
{
    static Dictionary<string, Entity> uidToEntityMap = new Dictionary<string, Entity>();
    static Dictionary<Entity, string> entityToUidMap = new Dictionary<Entity, string>();
    
    //-------------------------------------------------------------------------------------------------
    public static void RegisterEntity(Entity entity)
    {
        if (!entityToUidMap.ContainsKey(entity))
        {
            // it's a new entity
            if (string.IsNullOrEmpty(entity.uniqueId))
            {
                entity.uniqueId = Guid.NewGuid().ToString();
            }

            // it's a copy an existing entity
            if (uidToEntityMap.ContainsKey(entity.uniqueId))
            {
                entity.uniqueId = Guid.NewGuid().ToString();
            }

            entityToUidMap.Add(entity, entity.uniqueId);
            uidToEntityMap.Add(entity.uniqueId, entity);
        }
    }
    
    //-------------------------------------------------------------------------------------------------
    public static void UnregisterEntity(Entity entity)
    {
        entityToUidMap.Remove(entity);
        uidToEntityMap.Remove(entity.uniqueId);
    }    
    
    //-------------------------------------------------------------------------------------------------
    private static void RetrieveAllEntitiesInScene()
    {
        entityToUidMap.Clear();
        uidToEntityMap.Clear();
        
        var entities = UnityEngine.Object.FindObjectsOfType<Entity>();
        foreach (var entity in entities)
        {
            entityToUidMap.Add(entity, entity.uniqueId);
            uidToEntityMap.Add(entity.uniqueId, entity);
        }
    }        
    
}
