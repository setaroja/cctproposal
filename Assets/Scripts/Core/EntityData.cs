﻿using UnityEngine;

[CreateAssetMenu(menuName="Entity Data")]
public class EntityData : ScriptableObject
{
    public Entity.Archetype archetype;        
    public Entity.Category category;
    public bool isStatic;
}
