﻿
public interface ISaveLoad
{
    ComponentData GetSaveData();
    void SetSaveData(ComponentData componentData);
    bool IsModified();
}
