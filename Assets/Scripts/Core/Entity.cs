﻿using System.ComponentModel;
using Sirenix.OdinInspector;
using UnityEngine;

// check https://blogs.unity3d.com/es/2018/07/19/spotlight-team-best-practices-guid-based-references/
// for more intersteng ideas about GUIDs

[ExecuteInEditMode]
[SelectionBase]
public class Entity : MonoBehaviour, ISaveLoad
{
    public enum Archetype
    {
        Actionable,
        Interactuable,
        Decoration,
        Character
    }
    
    public enum Category
    {
        Decoration,
        Landmark,
        Character
    }

    class Data : ComponentData
    {
        public Vector3 position;
        public Vector3 rotation;
        public Vector3 scale;
    }
    
    [AssetsOnly]
    [Sirenix.OdinInspector.ReadOnly]
    [InlineEditor(InlineEditorModes.FullEditor)]
    [LabelText("Shared data")]
    [Required]
    public EntityData entityData;
    
    [SerializeField] [Sirenix.OdinInspector.ReadOnly] private string _uniqueId = null;

    private bool isModified = false;
    private Vector3 initialPosition;
    private Quaternion initialRotation;
    private Vector3 initialScale;

    //-------------------------------------------------------------------------------------------------
    public string uniqueId
    {
        get { return _uniqueId; }
        set { _uniqueId = value; }
    }

    //-------------------------------------------------------------------------------------------------
    private void Awake()
    {
        EntityManager.RegisterEntity(this);
        
        initialPosition = transform.position;
        initialRotation = transform.rotation;
        initialScale = transform.localScale;
    }

    //-------------------------------------------------------------------------------------------------
    private void OnDestroy()
    {
        EntityManager.UnregisterEntity(this);
    }

    //-------------------------------------------------------------------------------------------------
    public ComponentData GetSaveData()
    {
        Data data = new Data
        {
            position = transform.position,
            rotation = transform.rotation.eulerAngles,
            scale = transform.localScale
        };
        
        return data;
    }

    //-------------------------------------------------------------------------------------------------
    public void SetSaveData(ComponentData componentData)
    {
        var data = (Data) componentData;
        transform.position = data.position;
        transform.rotation = Quaternion.Euler(data.rotation);
        transform.localScale = data.scale;
        
        isModified = true;
    }

    //-------------------------------------------------------------------------------------------------
    public bool IsModified()
    {
        if (isModified)
            return true;
        if (entityData.isStatic)
            return false;
        if (initialPosition != transform.position ||
            initialRotation != transform.rotation ||
            initialScale != transform.localScale)
            return true;
            
        return false;
    }
    
    void OnValidate()
    {
        EntityManager.RegisterEntity(this);
        Debug.Log("OnValidate: " + uniqueId);
    }

    private void Reset()
    {
        Debug.Log("Reset: " + uniqueId);
    }
}
