﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class Spawner : MonoBehaviour
{
#if UNITY_EDITOR
	public Transform ground;
	public List<GameObject> prefabs;
	public int count = 500;
	public float radius = 100;
	public bool generate = false;	

	//-------------------------------------------------------------------------------------------------
	[ContextMenu ("Spawn!")]
	private void Spawn()
	{
		//radius = ground.GetComponent<MeshCollider>().bounds.max.x;
		for (int i = 0; i < count; i++)
		{
			int index = Random.Range(0, prefabs.Count);
			Vector3 pos = new Vector3(Random.Range(-radius, radius), 100, Random.Range(-radius, radius));

			int layerMask = 1 << 31;
			RaycastHit hit;
			if (Physics.Raycast(pos, -Vector3.up, out hit, 200, layerMask))
			{
				pos.y = hit.point.y;
				var go = PrefabUtility.InstantiatePrefab(prefabs[index]) as GameObject;
				go.transform.position = pos;
				var skinGroup = go.GetComponent<SkinGroup>();
				if (skinGroup)
				{
					skinGroup.SetRandomSkin();
				}
			}
		}
	}
#endif
}
