﻿using UnityEngine;

public class PanCamera : MonoBehaviour
{
    public float speed = 0.1f;
    public float inertia = .95f;

    private Vector3 lastMouseMosition;
    private Vector3 velocity = Vector3.zero;
    
    void Update() 
    {
        if (Input.GetMouseButtonDown(0))
        {
            lastMouseMosition = Input.mousePosition;
            velocity = Vector3.zero;
        }
        else if (Input.GetMouseButton(0))
        {
            var direction = Input.mousePosition - lastMouseMosition;
            velocity = new Vector3(-direction.y * speed, 0, direction.x * speed);
            lastMouseMosition = Input.mousePosition;
        }
        else
        {
            velocity *= inertia;
        }
        
        transform.Translate(velocity, Space.World);
        
        DetectTouchOnEntity();
    }
    
    //-------------------------------------------------------------------------------------------------
    void DetectTouchOnEntity()
    {
        if (
            //(Input.touchCount > 0) && (Input.GetTouch(0).phase == TouchPhase.Began) ||
            Input.GetMouseButtonDown(0))
        {
            Ray raycast = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit raycastHit;
            if (Physics.Raycast(raycast, out raycastHit, 1000, 1 << 30))
            {
                //if (raycastHit.collider.name == "Soccer")
                {
                    Debug.Log(raycastHit.collider.GetType().ToString() + " " + raycastHit.collider.name);
                    //transform.parent.SendMessage("OnTouch", SendMessageOptions.DontRequireReceiver );
                }
            }
        }
    }    
        
}
