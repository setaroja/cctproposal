﻿using System;
using System.IO;
using System.Net;
using UnityEngine;
using UnityEditor;
using Object = UnityEngine.Object;


public class EntityCreateWizard : ScriptableWizard
{
    public new string name = "Entity";
    public Entity.Archetype archetype;

    //-------------------------------------------------------------------------------------------------
    [MenuItem("GameObject/Create new CCT Entity")]
    static void CreateWizard()
    {
        ScriptableWizard.DisplayWizard<EntityCreateWizard>("Create Entity", "Create");
        
        AssetDatabase.CreateFolder("Assets", "Prefabs");
        CreateFolders(Entity.Archetype.Actionable);        
        CreateFolders(Entity.Archetype.Character);
        CreateFolders(Entity.Archetype.Decoration);
        CreateFolders(Entity.Archetype.Interactuable);
    }
    
    //-------------------------------------------------------------------------------------------------
    void OnWizardCreate()
    {
        GameObject newGo = new GameObject(name);;
        CreateAndSetStaticData(newGo, archetype);
        
        // forces uniqueId to null beacuse this is a prefab
        newGo.GetComponent<Entity>().uniqueId = null;
        
        var prefab = CreatePrefab(name, GetFolder(archetype), newGo);
        DestroyImmediate(newGo);

        AssetDatabase.SaveAssets ();
        AssetDatabase.Refresh();
        EditorUtility.FocusProjectWindow ();
        Selection.activeObject = prefab;
    }

    //-------------------------------------------------------------------------------------------------
    private void CreateAndSetStaticData(GameObject newGo, Entity.Archetype _archetype)
    {
        var entity = newGo.AddComponent<Entity>();
        entity.entityData = Resources.Load<EntityData>(_archetype.ToString());
        var skinGroup = newGo.AddComponent<SkinGroup>();

        skinGroup.skinGroupData = EditorUtils.CreateAsset<SkinGroupData>(GetFolder(_archetype) + "Data/" + newGo.name + "SkinGroup.asset");
    }    
    
    //-------------------------------------------------------------------------------------------------
    void OnWizardUpdate()
    {
        helpString = "Please set the color of the light!";
    }
    
    //-------------------------------------------------------------------------------------------------
    static Object CreatePrefab(string name, string path, GameObject go)
    {
        Object prefab = null;
        
        //Set the path as within the Assets folder, and name it as the GameObject's name with the .prefab format
        string localPath = path + "/" + go.name + ".prefab";

        //Check if the Prefab and/or name already exists at the path
        if (AssetDatabase.LoadAssetAtPath(localPath, typeof(GameObject)))
        {
            //Create dialog to ask if User is sure they want to overwrite existing prefab
            if (EditorUtility.DisplayDialog("Are you sure?",
                    "The prefab already exists. Do you want to overwrite it?",
                    "Yes",
                    "No"))
                //If the user presses the yes button, create the Prefab
            {
                prefab = CreateNew(go, localPath);
            }
        }
        //If the name doesn't exist, create the new Prefab
        else
        {
            prefab = CreateNew(go, localPath);
        }

        return prefab;
    }
    
    //-------------------------------------------------------------------------------------------------
    static Object CreateNew(GameObject obj, string localPath)
    {
        //Create a new prefab at the path given
        Object prefab = PrefabUtility.CreatePrefab(localPath, obj);
        PrefabUtility.ReplacePrefab(obj, prefab, ReplacePrefabOptions.ConnectToPrefab);

        return prefab;
    }

    //-------------------------------------------------------------------------------------------------
    static string GetFolder(Entity.Archetype _archetype)
    {
        string folder = "Assets/Prefabs/" + _archetype.ToString() + "s/";
        return folder;
    }
    
    //-------------------------------------------------------------------------------------------------
    static string GetFolderrelativeToAssets(Entity.Archetype _archetype)
    {
        string folder = "Prefabs/" + _archetype.ToString() + "s/";
        return folder;
    }

    //-------------------------------------------------------------------------------------------------
    static void CreateFolders(Entity.Archetype _archetype)
    {
        //AssetDatabase.CreateFolder("Assets/Prefabs", _archetype.ToString());
        //AssetDatabase.CreateFolder("Assets/Prefabs/" + _archetype.ToString(), "Data");        
    }
}
