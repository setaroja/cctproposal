﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

[InitializeOnLoad]
public class SceneEditor : EditorWindow
{
    private int currentPickerWindow;
    private static bool showSkins = true;
        

    //-------------------------------------------------------------------------------------------------
    static SceneEditor()
    {
        EditorApplication.playModeStateChanged += OnPlayModeStateChanged;
        EditorApplication.hierarchyChanged += OnHierarchyWindowChanged;
    }

    private static void OnHierarchyWindowChanged()
    {

    }


    //-------------------------------------------------------------------------------------------------
    private static void OnPlayModeStateChanged(PlayModeStateChange playModeState)
    {
        if (playModeState == PlayModeStateChange.ExitingEditMode)
        {
            DestroySkins();
        }
        if (playModeState == PlayModeStateChange.EnteredEditMode)
        {
            ShowSkins();
        }        
    }

    //-------------------------------------------------------------------------------------------------
    [MenuItem("CCT/Load and Save scene")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        SceneEditor window = (SceneEditor)EditorWindow.GetWindow(typeof(SceneEditor));
        window.titleContent = new GUIContent("Scene Editor");
        window.Show();
    }
    
    //-------------------------------------------------------------------------------------------------
    void OnGUI()
    {
        if (GUILayout.Button("Save scene"))
        {
            SaveScene();
        }
        
        if (GUILayout.Button("Load scene"))
        {
            
        }
        
        if (GUILayout.Button("Save game state"))
        {
            SaveLoadManager.Save();
        }
        
        if (GUILayout.Button("Load game state"))
        {
            SaveLoadManager.Load();
        }                
        
        if (GUILayout.Button("Create entity in scene"))
        {
            ShowPicker();
        }

        if (showSkins)
        {
            if (GUILayout.Button("Hide skins"))
            {
                HideSkins();
            }        
        }
        else if (GUILayout.Button("Show skins"))
        {
            ShowSkins();
        }        
        
    }

    //-------------------------------------------------------------------------------------------------
    private void SaveScene()
    {
        DestroySkins();

        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        
        ShowSkins();
    }

    //-------------------------------------------------------------------------------------------------
    private static void DestroySkins()
    {
        var skinGroups = FindObjectsOfType<SkinGroup>();
        foreach (var skinGroup in skinGroups)
        {
            skinGroup.UnloadSkin();
        }
    }
    
    //-------------------------------------------------------------------------------------------------
    private static void HideSkins()
    {
        var skinGroups = FindObjectsOfType<SkinGroup>();
        foreach (var skinGroup in skinGroups)
        {
            skinGroup.SetVisible(false);
        }
        
        showSkins = false;
    }    
    
    //-------------------------------------------------------------------------------------------------
    private static void ShowSkins()
    {
        var skinGroups = FindObjectsOfType<SkinGroup>();
        foreach (var skinGroup in skinGroups)
        {
            if (skinGroup.IsSkinLoaded())
            {
                skinGroup.SetVisible(true);
            }
            else
            {
                skinGroup.LoadSkin();
                skinGroup.SetVisible(true);
            }
        }
        
        showSkins = true;
    }    

    //-------------------------------------------------------------------------------------------------
    private void ShowPicker()
    {
        //create a window picker control ID
        currentPickerWindow = EditorGUIUtility.GetControlID(FocusType.Passive) + 100;
 
        //use the ID you just created
        EditorGUIUtility.ShowObjectPicker<GameObject>(null,false,"ee",currentPickerWindow);        
    }

    //-------------------------------------------------------------------------------------------------
    [MenuItem("CCT/Add component to entities")]
    public static void AddComponenToEntities()
    {
        string[] guids = AssetDatabase.FindAssets("", new[] {"Assets/Prefabs"});
        foreach (var guid in guids)
        {
            string assetPath = AssetDatabase.GUIDToAssetPath(guid);
            var entity = AssetDatabase.LoadAssetAtPath<Entity>(assetPath);
            if (entity)
            {
                Debug.Log(assetPath);

                GameObject obj = entity.gameObject;
                obj.AddComponent<SphereCollider>();
            }
        }

        AssetDatabase.SaveAssets ();
        AssetDatabase.Refresh();
    }
    
}
