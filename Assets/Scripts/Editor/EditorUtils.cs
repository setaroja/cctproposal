﻿using UnityEngine;
using UnityEditor;
using System.IO;
 
public static class EditorUtils
{
    //-------------------------------------------------------------------------------------------------
    public static T CreateAsset<T> (string assetPath) where T : ScriptableObject
    {
        T asset = ScriptableObject.CreateInstance<T> ();
 
        AssetDatabase.CreateAsset (asset, assetPath);
 
        AssetDatabase.SaveAssets ();
        AssetDatabase.Refresh();

        return asset;
    }
    
    //-------------------------------------------------------------------------------------------------
    public static bool IsPrefab(Object obj)
    {
        if (PrefabUtility.GetCorrespondingObjectFromSource(obj) == null && PrefabUtility.GetPrefabObject(obj) != null)
        {
            return true;
        }

        return false;
    }    
}