﻿using System.IO;
using NUnit.Framework;
using UnityEditor;
using UnityEngine;

public class AssetEventHandler : UnityEditor.AssetModificationProcessor
{
    //-------------------------------------------------------------------------------------------------
    public static AssetDeleteResult OnWillDeleteAsset(string assetPath, RemoveAssetOptions options)
    {
        var skinGroup = AssetDatabase.LoadAssetAtPath<SkinGroup>(assetPath);
        if (skinGroup)
        {
            Debug.Log("SkinGroup detected");
            string path, file;
            SplitPath(assetPath, out path, out file);
            string staticDataFilePath = path + "Data/" + Path.GetFileNameWithoutExtension(file) + "SkinGroup" + ".asset";
            
            DeleteAsset(staticDataFilePath);
            DeleteAsset(staticDataFilePath + ".meta");
            
            AssetDatabase.Refresh();
        }
        
        return AssetDeleteResult.DidNotDelete;
    }

    //-------------------------------------------------------------------------------------------------
    public static void SplitPath(string pathFileName, out string path, out string file)
    {
        path = pathFileName.Substring(0, pathFileName.LastIndexOf('/'));

        file = pathFileName.Substring(path.Length + 1);
        path += "/";
    }
    
    //-------------------------------------------------------------------------------------------------
    private static void DeleteAsset(string assetPath)
    {
        FileUtil.DeleteFileOrDirectory(assetPath);
        string metaFile = assetPath + ".meta";
        FileUtil.DeleteFileOrDirectory(metaFile);
    }
    
}
