﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITestController : UIController<UITestController>
{
	
	[AssetPath.Attribute(typeof(Object))]
	public List<string> m_Players;
	
	public Button button1;
	public Text text;

	private void Start()
	{
		Debug.Log(button1.name);
		Debug.Log(text.name);
	}
}
