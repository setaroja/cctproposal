﻿using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIController<T> : MonoBehaviour where T : UIController<T> 
{
#if UNITY_EDITOR
	
	
	void Reset()
	{
		FindControls();
	}

	[Button(ButtonSizes.Small)]
	void FindControls()
	{
		// For each field in your class/component we are looking only for those that are empty/null
		foreach (var field in typeof(T).GetFields().Where(field => field.GetValue(this) == null))
		{
			var component = transform.GetComponentInChildren(field.FieldType);
			if (component.name.ToLower() == field.Name.ToLower())
			{
				field.SetValue(this, component.GetComponent(field.FieldType));
			}
		}
	}

	public void OnButton1Press()
	{
		Debug.Log("Button 1 pressed");
	}

#endif
}